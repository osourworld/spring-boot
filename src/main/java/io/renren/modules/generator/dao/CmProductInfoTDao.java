package io.renren.modules.generator.dao;

import io.renren.modules.generator.entity.CmProductInfoTEntity;
import io.renren.modules.sys.dao.BaseDao;
import org.apache.ibatis.annotations.Mapper;

/**
 * 产品信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-09-16 16:59:21
 */
@Mapper
public interface CmProductInfoTDao extends BaseDao<CmProductInfoTEntity> {
	
	CmProductInfoTEntity equalCodeInfo(CmProductInfoTEntity param);
	
}
