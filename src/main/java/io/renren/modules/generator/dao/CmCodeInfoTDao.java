package io.renren.modules.generator.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import io.renren.modules.generator.entity.CmCodeInfoTEntity;
import io.renren.modules.sys.dao.BaseDao;

/**
 * 条码信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-09-16 16:59:21
 */
@Mapper
public interface CmCodeInfoTDao extends BaseDao<CmCodeInfoTEntity> {
	
	List<String> findCodeVersionList(Long userId);
	
	Integer findProductBatchNum(CmCodeInfoTEntity parm);
	
	Integer findOrderBatchNum(CmCodeInfoTEntity parm);
}
