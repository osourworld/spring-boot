package io.renren.modules.generator.dao;

import io.renren.modules.generator.entity.CmCodeRecordTEntity;
import io.renren.modules.sys.dao.BaseDao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 条码扫描记录表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-09-16 16:59:21
 */
@Mapper
public interface CmCodeRecordTDao extends BaseDao<CmCodeRecordTEntity> {
	
	CmCodeRecordTEntity checkCode(@Param("code")String code,@Param("createBy")Long createBy);
	
	List<CmCodeRecordTEntity> queryAllList(Map<String, Object> map);
	
	int queryAllListTotal(Map<String, Object> map);
	
}
