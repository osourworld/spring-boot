package io.renren.modules.generator.dao;

import io.renren.modules.generator.entity.CmOutlookSupplierTEntity;
import io.renren.modules.sys.dao.BaseDao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * 表面处理厂家
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-10-22 16:45:59
 */
@Mapper
public interface CmOutlookSupplierTDao extends BaseDao<CmOutlookSupplierTEntity> {
	
	List<Map<String, String>> getqueryList(Map<String, Object> map);
	
}
