package io.renren.modules.generator.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.generator.entity.CmProductNumTEntity;
import io.renren.modules.generator.service.CmProductNumTService;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.common.utils.R;
import io.renren.common.utils.ShiroUtils;




/**
 * 机台号表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-10-22 16:45:59
 */
@RestController
@RequestMapping("/generator/cmproductnumt")
public class CmProductNumTController {
	@Autowired
	private CmProductNumTService cmProductNumTService;
	
	/**
	 * 列表
	 */
	@RequestMapping("/list")
	@RequiresPermissions("generator:cmproductnumt:list")
	public R list(@RequestParam Map<String, Object> params){
		//当前登录人
      	SysUserEntity userVO=ShiroUtils.getUserEntity();
      	Long userId=userVO.getUserId();
      	params.put("createBy", userId);
		//查询列表数据
        Query query = new Query(params);

		List<CmProductNumTEntity> cmProductNumTList = cmProductNumTService.queryList(query);
		int total = cmProductNumTService.queryTotal(query);
		
		PageUtils pageUtil = new PageUtils(cmProductNumTList, total, query.getLimit(), query.getPage());
		
		return R.ok().put("page", pageUtil);
	}
	
	/**
	 * 列表
	 */
	@RequestMapping("/getlist")
	//@RequiresPermissions("generator:cmoutlooksuppliert:list")
	public R getlist(){
		Map<String, Object> params=new HashMap<String,Object>();
        //当前登录人
      	SysUserEntity userVO=ShiroUtils.getUserEntity();
      	Long userId=userVO.getUserId();
      	params.put("createBy", userId);

      	List<Map<String, String>> cmOutlookSupplierTList = cmProductNumTService.getqueryList(params);
		
		
		return R.ok().put("list", cmOutlookSupplierTList);
	}
	
	/**
	 * 信息
	 */
	@RequestMapping("/info/{productNumId}")
	@RequiresPermissions("generator:cmproductnumt:info")
	public R info(@PathVariable("productNumId") Long productNumId){
		CmProductNumTEntity cmProductNumT = cmProductNumTService.queryObject(productNumId);
		
		return R.ok().put("cmProductNumT", cmProductNumT);
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("/save")
	@RequiresPermissions("generator:cmproductnumt:save")
	public R save(@RequestBody CmProductNumTEntity cmProductNumT){
		//当前登录人
      	SysUserEntity userVO=ShiroUtils.getUserEntity();
      	Long userId=userVO.getUserId();
      	cmProductNumT.setCreateBy(userId);
      	cmProductNumT.setCreateTime(new Date());
		cmProductNumTService.save(cmProductNumT);
		
		return R.ok();
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	@RequiresPermissions("generator:cmproductnumt:update")
	public R update(@RequestBody CmProductNumTEntity cmProductNumT){
		cmProductNumTService.update(cmProductNumT);
		
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	@RequiresPermissions("generator:cmproductnumt:delete")
	public R delete(@RequestBody Long[] productNumIds){
		cmProductNumTService.deleteBatch(productNumIds);
		
		return R.ok();
	}
	
}
