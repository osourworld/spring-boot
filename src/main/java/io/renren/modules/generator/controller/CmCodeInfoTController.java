package io.renren.modules.generator.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.common.utils.R;
import io.renren.common.utils.ShiroUtils;
import io.renren.modules.generator.entity.CmCodeInfoTEntity;
import io.renren.modules.generator.service.CmCodeInfoTService;
import io.renren.modules.sys.entity.SysUserEntity;




/**
 * 条码信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-09-16 16:59:21
 */
@RestController
@RequestMapping("/generator/cmcodeinfot")
public class CmCodeInfoTController {
	@Autowired
	private CmCodeInfoTService cmCodeInfoTService;
	
	/**
	 * 列表
	 */
	@RequestMapping("/list")
	//@RequiresPermissions("generator:cmcodeinfot:list")
	public R list(@RequestParam Map<String, Object> params){
		//当前登录人
		SysUserEntity userVO=ShiroUtils.getUserEntity();
		Long userId=userVO.getUserId();
		params.put("createBy", userId);
		//查询列表数据
        Query query = new Query(params);

		List<CmCodeInfoTEntity> cmCodeInfoTList = cmCodeInfoTService.queryList(query);
		int total = cmCodeInfoTService.queryTotal(query);
		
		PageUtils pageUtil = new PageUtils(cmCodeInfoTList, total, query.getLimit(), query.getPage());
		
		return R.ok().put("page", pageUtil);
	}
	
	/**
	 * 列表
	 */
	@RequestMapping("/listAll")
	//@RequiresPermissions("generator:cmcodeinfot:list")
	public R listAll(@RequestParam Map<String, Object> params){
		//查询列表数据
        Query query = new Query(params);

		List<CmCodeInfoTEntity> cmCodeInfoTList = cmCodeInfoTService.queryList(query);
		int total = cmCodeInfoTService.queryTotal(query);
		
		PageUtils pageUtil = new PageUtils(cmCodeInfoTList, total, query.getLimit(), query.getPage());
		
		return R.ok().put("page", pageUtil);
	}
	
	
	/**
	 * 信息
	 */
	@RequestMapping("/info/{codeId}")
	//@RequiresPermissions("generator:cmcodeinfot:info")
	public R info(@PathVariable("codeId") Long codeId){
		CmCodeInfoTEntity cmCodeInfoT = cmCodeInfoTService.queryObject(codeId);
		
		return R.ok().put("cmCodeInfoT", cmCodeInfoT);
	}
	
	/**
	 * 保存
	 * @throws ParseException 
	 */
	@RequestMapping("/save")
	//@RequiresPermissions("generator:cmcodeinfot:save")
	public R save(@RequestBody CmCodeInfoTEntity cmCodeInfoT) throws ParseException{
		//当前登录人
		SysUserEntity userVO=ShiroUtils.getUserEntity();
		Long userId=userVO.getUserId();
		cmCodeInfoT.setCreateBy(userId);
		String productTime=cmCodeInfoT.getProductTimeStr();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		Date productDate = formatter.parse(productTime);
		cmCodeInfoT.setProductTime(productDate);
		String supplierCode=cmCodeInfoT.getSupplierCode();
		String materialCode=cmCodeInfoT.getMaterialCode();
		String materialVersion=cmCodeInfoT.getMaterialVersion();
		String productDateStr=cmCodeInfoT.getProductDate();
		String orderDateStr=cmCodeInfoT.getOrderDate();
		
		//查询生产流水号
		Integer productNum = cmCodeInfoTService.findProductBatchNum(cmCodeInfoT);
		if(productNum!=null){
			productNum++;
		}else{
			productNum=1;
		}
		String code="";
		code=supplierCode+materialCode+materialVersion+productDateStr+this.pad(productNum, 5);
		//查询出货流水号
		Integer orderNum = cmCodeInfoTService.findProductBatchNum(cmCodeInfoT);
		if(orderNum!=null){
			orderNum++;
		}else{
			orderNum=1;
		}
		String code2="";
		code2=supplierCode+materialCode+materialVersion+orderDateStr+this.pad(orderNum, 3);
		
		code=code.toUpperCase();
		String ragex = "\\.|-";
		code=code.replaceAll(ragex, "");
		code2=code2.toUpperCase();
		code2=code2.replaceAll(ragex, "");
		cmCodeInfoT.setCode(code);
		cmCodeInfoT.setCode2(code2);
		cmCodeInfoT.setProductBatchNum(productNum);
		cmCodeInfoT.setOrderBatchNum(orderNum);
		cmCodeInfoTService.save(cmCodeInfoT);
		
		return R.ok().put("code1", code).put("code2", code2);
	}
	
	private String pad(Integer num,Integer n){
		String number=num+"";
		Integer len = number.length();
		while(len < n) {
			number = "0" + number;
			len++;
		}
		return number;
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	//@RequiresPermissions("generator:cmcodeinfot:update")
	public R update(@RequestBody CmCodeInfoTEntity cmCodeInfoT){
		cmCodeInfoTService.update(cmCodeInfoT);
		
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	//@RequiresPermissions("generator:cmcodeinfot:delete")
	public R delete(@RequestBody Long[] codeIds){
		cmCodeInfoTService.deleteBatch(codeIds);
		
		return R.ok();
	}
	
	/**
	 * 查询编码版本
	 */
	@RequestMapping("/codeversionlist")
	//@RequiresPermissions("generator:cmcodeinfot:list")
	public R findCodeVersionList(@RequestParam Map<String, Object> params){
		//当前登录人
		SysUserEntity userVO=ShiroUtils.getUserEntity();
		Long userId=userVO.getUserId();
		List<String> codeVersionList = cmCodeInfoTService.findCodeVersionList(userId);
		
		return R.ok().put("result", codeVersionList);
	}
	
	/**
	 * 查询流水号
	 */
	@RequestMapping("/findBatchNum")
	//@RequiresPermissions("generator:cmcodeinfot:list")
	public R findBatchNum(@RequestBody CmCodeInfoTEntity cmCodeInfoT){
		//当前登录人
		SysUserEntity userVO=ShiroUtils.getUserEntity();
		Long userId=userVO.getUserId();
		cmCodeInfoT.setCreateBy(userId);
		Integer productNum = cmCodeInfoTService.findProductBatchNum(cmCodeInfoT);
		Integer orderNum = cmCodeInfoTService.findProductBatchNum(cmCodeInfoT);
		return R.ok().put("productNum", productNum).put("orderNum", orderNum);
	}
	
}
