package io.renren.modules.generator.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.generator.service.CmCodeInfoTService;
import io.renren.modules.generator.utils.GenerateQrCodeUtil;




/**
 * 支付
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-09-16 16:59:21
 */
@RestController
@RequestMapping("/generator/paywx")
public class CmPayController {
	@Autowired
	private CmCodeInfoTService cmCodeInfoTService;
	
	/**
	   * 生成二维码图片并直接以流的形式输出到页面
	   * @param code_url
	   * @param response
	   */
	  @RequestMapping("qr_code")
	  @ResponseBody
	  public void getQRCode(String code_url,HttpServletResponse response){
	    GenerateQrCodeUtil.encodeQrcode(code_url, response);
	  }
	
	
	
}
