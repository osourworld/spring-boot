package io.renren.modules.generator.controller;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.common.utils.R;
import io.renren.common.utils.ShiroUtils;
import io.renren.modules.generator.entity.CmProductInfoTEntity;
import io.renren.modules.generator.service.CmProductInfoTService;
import io.renren.modules.oss.controller.SysOssController;
import io.renren.modules.sys.entity.SysUserEntity;




/**
 * 产品信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-09-16 16:59:21
 */
@RestController
@RequestMapping("/generator/cmproductinfot")
public class CmProductInfoTController {
	@Autowired
	private CmProductInfoTService cmProductInfoTService;
	
	/**
	 * 列表
	 */
	@RequestMapping("/list")
	//@RequiresPermissions("generator:cmproductinfot:list")
	public R list(@RequestParam Map<String, Object> params){
		//当前登录人
		SysUserEntity userVO=ShiroUtils.getUserEntity();
		Long userId=userVO.getUserId();
		params.put("createBy", userId);
		//查询列表数据
        Query query = new Query(params);

		List<CmProductInfoTEntity> cmProductInfoTList = cmProductInfoTService.queryList(query);
		int total = cmProductInfoTService.queryTotal(query);
		
		PageUtils pageUtil = new PageUtils(cmProductInfoTList, total, query.getLimit(), query.getPage());
		
		return R.ok().put("page", pageUtil);
	}
	
	
	/**
	 * 信息
	 */
	@RequestMapping("/info/{productId}")
	//@RequiresPermissions("generator:cmproductinfot:info")
	public R info(@PathVariable("productId") Long productId){
		CmProductInfoTEntity cmProductInfoT = cmProductInfoTService.queryObject(productId);
		
		return R.ok().put("cmProductInfoT", cmProductInfoT);
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("/save")
	//@RequiresPermissions("generator:cmproductinfot:save")
	public R save(@RequestBody CmProductInfoTEntity cmProductInfoT){
		//当前登录人
		SysUserEntity userVO=ShiroUtils.getUserEntity();
		Long userId=userVO.getUserId();
		cmProductInfoT.setCreateBy(userId);
		cmProductInfoTService.save(cmProductInfoT);
		
		return R.ok();
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	//@RequiresPermissions("generator:cmproductinfot:update")
	public R update(@RequestBody CmProductInfoTEntity cmProductInfoT){
		//当前登录人
		SysUserEntity userVO=ShiroUtils.getUserEntity();
		Long userId=userVO.getUserId();
		cmProductInfoT.setCreateBy(userId);
		cmProductInfoTService.update(cmProductInfoT);
		
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	//@RequiresPermissions("generator:cmproductinfot:delete")
	public R delete(@RequestBody Long[] productIds){
		cmProductInfoTService.deleteBatch(productIds);
		
		return R.ok();
	}
	
	
	/**
	 * 信息
	 */
	@RequestMapping("/equalCodeInfo")
	//@RequiresPermissions("generator:cmproductinfot:info")
	public R equalCodeInfo(@RequestBody CmProductInfoTEntity cmProductInfoT){
		//当前登录人
		SysUserEntity userVO=ShiroUtils.getUserEntity();
		Long userId=userVO.getUserId();
		cmProductInfoT.setCreateBy(userId);
		CmProductInfoTEntity result = cmProductInfoTService.equalCodeInfo(cmProductInfoT);
		return R.ok().put("cmProductInfoT", result);
	}
	
}
