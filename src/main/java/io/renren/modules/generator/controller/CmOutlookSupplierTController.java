package io.renren.modules.generator.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.generator.entity.CmOutlookSupplierTEntity;
import io.renren.modules.generator.service.CmOutlookSupplierTService;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.common.utils.R;
import io.renren.common.utils.ShiroUtils;




/**
 * 表面处理厂家
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-10-22 16:45:59
 */
@RestController
@RequestMapping("/generator/cmoutlooksuppliert")
public class CmOutlookSupplierTController {
	@Autowired
	private CmOutlookSupplierTService cmOutlookSupplierTService;
	
	/**
	 * 列表
	 */
	@RequestMapping("/list")
	//@RequiresPermissions("generator:cmoutlooksuppliert:list")
	public R list(@RequestParam Map<String, Object> params){
		//当前登录人
      	SysUserEntity userVO=ShiroUtils.getUserEntity();
      	Long userId=userVO.getUserId();
      	params.put("createBy", userId);
		//查询列表数据
        Query query = new Query(params);
		List<CmOutlookSupplierTEntity> cmOutlookSupplierTList = cmOutlookSupplierTService.queryList(query);
		int total = cmOutlookSupplierTService.queryTotal(query);
		
		PageUtils pageUtil = new PageUtils(cmOutlookSupplierTList, total, query.getLimit(), query.getPage());
		
		return R.ok().put("page", pageUtil);
	}
	
	/**
	 * 列表
	 */
	@RequestMapping("/getlist")
	//@RequiresPermissions("generator:cmoutlooksuppliert:list")
	public R getlist(){
		Map<String, Object> params=new HashMap<String,Object>();
        //当前登录人
      	SysUserEntity userVO=ShiroUtils.getUserEntity();
      	Long userId=userVO.getUserId();
      	params.put("createBy", userId);

      	List<Map<String, String>> cmOutlookSupplierTList = cmOutlookSupplierTService.getqueryList(params);
		
		
		return R.ok().put("list", cmOutlookSupplierTList);
	}
	
	
	/**
	 * 信息
	 */
	@RequestMapping("/info/{outlookSupplierId}")
	@RequiresPermissions("generator:cmoutlooksuppliert:info")
	public R info(@PathVariable("outlookSupplierId") Long outlookSupplierId){
		CmOutlookSupplierTEntity cmOutlookSupplierT = cmOutlookSupplierTService.queryObject(outlookSupplierId);
		
		return R.ok().put("cmOutlookSupplierT", cmOutlookSupplierT);
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("/save")
	@RequiresPermissions("generator:cmoutlooksuppliert:save")
	public R save(@RequestBody CmOutlookSupplierTEntity cmOutlookSupplierT){
		//当前登录人
      	SysUserEntity userVO=ShiroUtils.getUserEntity();
      	Long userId=userVO.getUserId();
      	cmOutlookSupplierT.setCreateBy(userId);
      	cmOutlookSupplierT.setCreateTime(new Date());
		cmOutlookSupplierTService.save(cmOutlookSupplierT);
		
		return R.ok();
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	@RequiresPermissions("generator:cmoutlooksuppliert:update")
	public R update(@RequestBody CmOutlookSupplierTEntity cmOutlookSupplierT){
		cmOutlookSupplierTService.update(cmOutlookSupplierT);
		
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	@RequiresPermissions("generator:cmoutlooksuppliert:delete")
	public R delete(@RequestBody Long[] outlookSupplierIds){
		cmOutlookSupplierTService.deleteBatch(outlookSupplierIds);
		
		return R.ok();
	}
	
}
