package io.renren.modules.generator.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.druid.util.StringUtils;

import io.renren.modules.generator.entity.CmCodeRecordTEntity;
import io.renren.modules.generator.service.CmCodeRecordTService;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.common.utils.PageUtils;
import io.renren.common.utils.Query;
import io.renren.common.utils.R;
import io.renren.common.utils.ShiroUtils;




/**
 * 条码扫描记录表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-09-16 16:59:21
 */
@RestController
@RequestMapping("/generator/cmcoderecordt")
public class CmCodeRecordTController {
	@Autowired
	private CmCodeRecordTService cmCodeRecordTService;
	
	/**
	 * 列表
	 * @throws ParseException 
	 */
	@RequestMapping("/list")
	//@RequiresPermissions("generator:cmcoderecordt:list")
	public R list(@RequestParam Map<String, Object> params) throws ParseException{
		//当前登录人
		SysUserEntity userVO=ShiroUtils.getUserEntity();
		Long userId=userVO.getUserId();
		params.put("createBy", userId);
		
		String startTime=(String) params.get("startTime");
		String endTime=(String) params.get("endTime");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(startTime!=null && !"".equals(startTime)){
			Date startDate=formatter.parse(startTime+" 00:00:00");
			params.put("startDate", startDate);
		}
		if(endTime!=null && !"".equals(endTime)){
			Date endDate=formatter.parse(endTime+" 23:59:59");
			params.put("endDate", endDate);
		}
		
		
		
		
		//查询列表数据
        Query query = new Query(params);

		List<CmCodeRecordTEntity> cmCodeRecordTList = cmCodeRecordTService.queryList(query);
		int total = cmCodeRecordTService.queryTotal(query);
		
		PageUtils pageUtil = new PageUtils(cmCodeRecordTList, total, query.getLimit(), query.getPage());
		
		return R.ok().put("page", pageUtil);
	}
	
	/**
	 * 列表
	 * @throws ParseException 
	 */
	@RequestMapping("/getAlllist")
	//@RequiresPermissions("generator:cmcoderecordt:list")
	public R getAlllist(@RequestParam Map<String, Object> params) throws ParseException{
		params.put("createBy", (Long) params.get("userId"));
		String startTime=(String) params.get("startTime");
		String endTime=(String) params.get("endTime");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(startTime!=null && !"".equals(startTime)){
			Date startDate=formatter.parse(startTime+" 00:00:00");
			params.put("startDate", startDate);
		}
		if(endTime!=null && !"".equals(endTime)){
			Date endDate=formatter.parse(endTime+" 23:59:59");
			params.put("endDate", endDate);
		}
		//查询列表数据
        Query query = new Query(params);

		List<CmCodeRecordTEntity> cmCodeRecordTList = cmCodeRecordTService.queryAllList(query);
		int total = cmCodeRecordTService.queryAllTotal(query);
		
		PageUtils pageUtil = new PageUtils(cmCodeRecordTList, total, query.getLimit(), query.getPage());
		
		return R.ok().put("page", pageUtil);
	}
	
	
	
	/**
	 * 信息
	 */
	@RequestMapping("/info/{codeRecordId}")
	//@RequiresPermissions("generator:cmcoderecordt:info")
	public R info(@PathVariable("codeRecordId") Long codeRecordId){
		CmCodeRecordTEntity cmCodeRecordT = cmCodeRecordTService.queryObject(codeRecordId);
		
		return R.ok().put("cmCodeRecordT", cmCodeRecordT);
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("/save")
	//@RequiresPermissions("generator:cmcoderecordt:save")
	public R save(@RequestBody CmCodeRecordTEntity cmCodeRecordT){
		//当前登录人
		SysUserEntity userVO=ShiroUtils.getUserEntity();
		Long userId=userVO.getUserId();
		cmCodeRecordT.setCreateBy(userId);
		cmCodeRecordTService.save(cmCodeRecordT);
		
		return R.ok();
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	//@RequiresPermissions("generator:cmcoderecordt:update")
	public R update(@RequestBody CmCodeRecordTEntity cmCodeRecordT){
		cmCodeRecordTService.update(cmCodeRecordT);
		
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@RequestMapping("/delete")
	//@RequiresPermissions("generator:cmcoderecordt:delete")
	public R delete(@RequestBody Long[] codeRecordIds){
		cmCodeRecordTService.deleteBatch(codeRecordIds);
		
		return R.ok();
	}
	
	/**
	 * 检验重复信息
	 */
	@RequestMapping("/checkinfo")
	//@RequiresPermissions("generator:cmcoderecordt:info")
	public R checkCode(@RequestBody CmCodeRecordTEntity cmCodeRecordT){
		int saveStatus=0;
		int count=0;
		String time="";
		//当前登录人
		SysUserEntity userVO=ShiroUtils.getUserEntity();
		Long userId=userVO.getUserId();
		cmCodeRecordT.setCreateBy(userId);
		String code=cmCodeRecordT.getCode();
		if(StringUtils.isEmpty(code)){
			return R.ok().put("codeCount", -1).put("saveStatus", saveStatus);
		}
		CmCodeRecordTEntity recordVO = cmCodeRecordTService.checkCode(code,userId);
		if(recordVO==null){
			//保存
			cmCodeRecordTService.save(cmCodeRecordT);
			saveStatus=1;
		}else{
			Date creatDate=recordVO.getCreateTime();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			time=formatter.format(creatDate);
			count=1;
		}
		
		return R.ok().put("codeCount", count).put("saveStatus", saveStatus).put("time", time);
	}
	
}
