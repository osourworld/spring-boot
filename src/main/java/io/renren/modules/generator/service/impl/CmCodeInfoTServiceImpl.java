package io.renren.modules.generator.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.renren.modules.generator.dao.CmCodeInfoTDao;
import io.renren.modules.generator.entity.CmCodeInfoTEntity;
import io.renren.modules.generator.service.CmCodeInfoTService;



@Service("cmCodeInfoTService")
public class CmCodeInfoTServiceImpl implements CmCodeInfoTService {
	@Autowired
	private CmCodeInfoTDao cmCodeInfoTDao;
	
	@Override
	public CmCodeInfoTEntity queryObject(Long codeId){
		return cmCodeInfoTDao.queryObject(codeId);
	}
	
	@Override
	public List<CmCodeInfoTEntity> queryList(Map<String, Object> map){
		return cmCodeInfoTDao.queryList(map);
	}
	
	@Override
	public int queryTotal(Map<String, Object> map){
		return cmCodeInfoTDao.queryTotal(map);
	}
	
	@Override
	public void save(CmCodeInfoTEntity cmCodeInfoT){
		cmCodeInfoTDao.save(cmCodeInfoT);
	}
	
	@Override
	public void update(CmCodeInfoTEntity cmCodeInfoT){
		cmCodeInfoTDao.update(cmCodeInfoT);
	}
	
	@Override
	public void delete(Long codeId){
		cmCodeInfoTDao.delete(codeId);
	}
	
	@Override
	public void deleteBatch(Long[] codeIds){
		cmCodeInfoTDao.deleteBatch(codeIds);
	}

	@Override
	public List<String> findCodeVersionList(Long userId) {
		return cmCodeInfoTDao.findCodeVersionList(userId);
	}

	@Override
	public Integer findProductBatchNum(CmCodeInfoTEntity parm) {
		
		return cmCodeInfoTDao.findProductBatchNum(parm);
	}

	@Override
	public Integer findOrderBatchNum(CmCodeInfoTEntity cmCodeInfoT) {
		
		return cmCodeInfoTDao.findOrderBatchNum(cmCodeInfoT);
	}
	
}
