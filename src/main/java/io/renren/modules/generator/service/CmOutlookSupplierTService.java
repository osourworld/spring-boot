package io.renren.modules.generator.service;

import io.renren.modules.generator.entity.CmOutlookSupplierTEntity;

import java.util.List;
import java.util.Map;

/**
 * 表面处理厂家
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-10-22 16:45:59
 */
public interface CmOutlookSupplierTService {
	
	CmOutlookSupplierTEntity queryObject(Long outlookSupplierId);
	
	List<CmOutlookSupplierTEntity> queryList(Map<String, Object> map);
	
	List<Map<String, String>> getqueryList(Map<String, Object> map);
	
	int queryTotal(Map<String, Object> map);
	
	void save(CmOutlookSupplierTEntity cmOutlookSupplierT);
	
	void update(CmOutlookSupplierTEntity cmOutlookSupplierT);
	
	void delete(Long outlookSupplierId);
	
	void deleteBatch(Long[] outlookSupplierIds);
}
