package io.renren.modules.generator.service;

import io.renren.modules.generator.entity.CmProductInfoTEntity;

import java.util.List;
import java.util.Map;

/**
 * 产品信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-09-16 16:59:21
 */
public interface CmProductInfoTService {
	
	CmProductInfoTEntity queryObject(Long productId);
	
	List<CmProductInfoTEntity> queryList(Map<String, Object> map);
	
	int queryTotal(Map<String, Object> map);
	
	void save(CmProductInfoTEntity cmProductInfoT);
	
	void update(CmProductInfoTEntity cmProductInfoT);
	
	void delete(Long productId);
	
	void deleteBatch(Long[] productIds);
	
	CmProductInfoTEntity equalCodeInfo(CmProductInfoTEntity param);
}
