package io.renren.modules.generator.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import io.renren.modules.generator.dao.CmProductInfoTDao;
import io.renren.modules.generator.entity.CmProductInfoTEntity;
import io.renren.modules.generator.service.CmProductInfoTService;



@Service("cmProductInfoTService")
public class CmProductInfoTServiceImpl implements CmProductInfoTService {
	@Autowired
	private CmProductInfoTDao cmProductInfoTDao;
	
	@Override
	public CmProductInfoTEntity queryObject(Long productId){
		return cmProductInfoTDao.queryObject(productId);
	}
	
	@Override
	public List<CmProductInfoTEntity> queryList(Map<String, Object> map){
		return cmProductInfoTDao.queryList(map);
	}
	
	@Override
	public int queryTotal(Map<String, Object> map){
		return cmProductInfoTDao.queryTotal(map);
	}
	
	@Override
	public void save(CmProductInfoTEntity cmProductInfoT){
		cmProductInfoTDao.save(cmProductInfoT);
	}
	
	@Override
	public void update(CmProductInfoTEntity cmProductInfoT){
		cmProductInfoTDao.update(cmProductInfoT);
	}
	
	@Override
	public void delete(Long productId){
		cmProductInfoTDao.delete(productId);
	}
	
	@Override
	public void deleteBatch(Long[] productIds){
		cmProductInfoTDao.deleteBatch(productIds);
	}

	@Override
	public CmProductInfoTEntity equalCodeInfo(CmProductInfoTEntity param) {
		return cmProductInfoTDao.equalCodeInfo(param);
	}
	
}
