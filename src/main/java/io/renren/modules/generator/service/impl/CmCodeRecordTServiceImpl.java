package io.renren.modules.generator.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import io.renren.modules.generator.dao.CmCodeRecordTDao;
import io.renren.modules.generator.entity.CmCodeRecordTEntity;
import io.renren.modules.generator.service.CmCodeRecordTService;



@Service("cmCodeRecordTService")
public class CmCodeRecordTServiceImpl implements CmCodeRecordTService {
	@Autowired
	private CmCodeRecordTDao cmCodeRecordTDao;
	
	@Override
	public CmCodeRecordTEntity queryObject(Long codeRecordId){
		return cmCodeRecordTDao.queryObject(codeRecordId);
	}
	
	@Override
	public List<CmCodeRecordTEntity> queryList(Map<String, Object> map){
		return cmCodeRecordTDao.queryList(map);
	}
	
	@Override
	public List<CmCodeRecordTEntity> queryAllList(Map<String, Object> map){
		return cmCodeRecordTDao.queryAllList(map);
	}
	
	@Override
	public int queryAllTotal(Map<String, Object> map){
		return cmCodeRecordTDao.queryAllListTotal(map);
	}
	
	@Override
	public int queryTotal(Map<String, Object> map){
		return cmCodeRecordTDao.queryTotal(map);
	}
	
	@Override
	public void save(CmCodeRecordTEntity cmCodeRecordT){
		cmCodeRecordTDao.save(cmCodeRecordT);
	}
	
	@Override
	public void update(CmCodeRecordTEntity cmCodeRecordT){
		cmCodeRecordTDao.update(cmCodeRecordT);
	}
	
	@Override
	public void delete(Long codeRecordId){
		cmCodeRecordTDao.delete(codeRecordId);
	}
	
	@Override
	public void deleteBatch(Long[] codeRecordIds){
		cmCodeRecordTDao.deleteBatch(codeRecordIds);
	}

	@Override
	public CmCodeRecordTEntity checkCode(String code,Long createBy) {
		return cmCodeRecordTDao.checkCode(code,createBy);
	}
	
}
