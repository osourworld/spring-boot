package io.renren.modules.generator.service;

import io.renren.modules.generator.entity.CmCodeRecordTEntity;

import java.util.List;
import java.util.Map;

/**
 * 条码扫描记录表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-09-16 16:59:21
 */
public interface CmCodeRecordTService {
	
	CmCodeRecordTEntity queryObject(Long codeRecordId);
	
	List<CmCodeRecordTEntity> queryList(Map<String, Object> map);
	
	List<CmCodeRecordTEntity> queryAllList(Map<String, Object> map);
	
	int queryAllTotal(Map<String, Object> map);
	
	int queryTotal(Map<String, Object> map);
	
	void save(CmCodeRecordTEntity cmCodeRecordT);
	
	void update(CmCodeRecordTEntity cmCodeRecordT);
	
	void delete(Long codeRecordId);
	
	void deleteBatch(Long[] codeRecordIds);
	
	CmCodeRecordTEntity checkCode(String code,Long createBy);
}
