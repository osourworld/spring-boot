package io.renren.modules.generator.service;

import io.renren.modules.generator.entity.CmProductNumTEntity;

import java.util.List;
import java.util.Map;

/**
 * 机台号表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-10-22 16:45:59
 */
public interface CmProductNumTService {
	
	CmProductNumTEntity queryObject(Long productNumId);
	
	List<CmProductNumTEntity> queryList(Map<String, Object> map);
	
	List<Map<String, String>> getqueryList(Map<String, Object> map);
	
	int queryTotal(Map<String, Object> map);
	
	void save(CmProductNumTEntity cmProductNumT);
	
	void update(CmProductNumTEntity cmProductNumT);
	
	void delete(Long productNumId);
	
	void deleteBatch(Long[] productNumIds);
}
