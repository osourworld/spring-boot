package io.renren.modules.generator.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import io.renren.modules.generator.dao.CmOutlookSupplierTDao;
import io.renren.modules.generator.entity.CmOutlookSupplierTEntity;
import io.renren.modules.generator.service.CmOutlookSupplierTService;



@Service("cmOutlookSupplierTService")
public class CmOutlookSupplierTServiceImpl implements CmOutlookSupplierTService {
	@Autowired
	private CmOutlookSupplierTDao cmOutlookSupplierTDao;
	
	@Override
	public CmOutlookSupplierTEntity queryObject(Long outlookSupplierId){
		return cmOutlookSupplierTDao.queryObject(outlookSupplierId);
	}
	
	@Override
	public List<CmOutlookSupplierTEntity> queryList(Map<String, Object> map){
		return cmOutlookSupplierTDao.queryList(map);
	}
	
	@Override
	public int queryTotal(Map<String, Object> map){
		return cmOutlookSupplierTDao.queryTotal(map);
	}
	
	@Override
	public void save(CmOutlookSupplierTEntity cmOutlookSupplierT){
		cmOutlookSupplierTDao.save(cmOutlookSupplierT);
	}
	
	@Override
	public void update(CmOutlookSupplierTEntity cmOutlookSupplierT){
		cmOutlookSupplierTDao.update(cmOutlookSupplierT);
	}
	
	@Override
	public void delete(Long outlookSupplierId){
		cmOutlookSupplierTDao.delete(outlookSupplierId);
	}
	
	@Override
	public void deleteBatch(Long[] outlookSupplierIds){
		cmOutlookSupplierTDao.deleteBatch(outlookSupplierIds);
	}

	@Override
	public List<Map<String, String>> getqueryList(Map<String, Object> map) {
		return cmOutlookSupplierTDao.getqueryList(map);
	}
	
}
