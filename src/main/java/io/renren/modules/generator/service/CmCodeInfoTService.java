package io.renren.modules.generator.service;

import io.renren.modules.generator.entity.CmCodeInfoTEntity;

import java.util.List;
import java.util.Map;

/**
 * 条码信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-09-16 16:59:21
 */
public interface CmCodeInfoTService {
	
	CmCodeInfoTEntity queryObject(Long codeId);
	
	List<CmCodeInfoTEntity> queryList(Map<String, Object> map);
	
	int queryTotal(Map<String, Object> map);
	
	void save(CmCodeInfoTEntity cmCodeInfoT);
	
	void update(CmCodeInfoTEntity cmCodeInfoT);
	
	void delete(Long codeId);
	
	void deleteBatch(Long[] codeIds);
	
	List<String> findCodeVersionList(Long userId);
	
	Integer findProductBatchNum(CmCodeInfoTEntity cmCodeInfoT);
	
	Integer findOrderBatchNum(CmCodeInfoTEntity cmCodeInfoT);
}
