package io.renren.modules.generator.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import io.renren.modules.generator.dao.CmProductNumTDao;
import io.renren.modules.generator.entity.CmProductNumTEntity;
import io.renren.modules.generator.service.CmProductNumTService;



@Service("cmProductNumTService")
public class CmProductNumTServiceImpl implements CmProductNumTService {
	@Autowired
	private CmProductNumTDao cmProductNumTDao;
	
	@Override
	public CmProductNumTEntity queryObject(Long productNumId){
		return cmProductNumTDao.queryObject(productNumId);
	}
	
	@Override
	public List<CmProductNumTEntity> queryList(Map<String, Object> map){
		return cmProductNumTDao.queryList(map);
	}
	
	@Override
	public int queryTotal(Map<String, Object> map){
		return cmProductNumTDao.queryTotal(map);
	}
	
	@Override
	public void save(CmProductNumTEntity cmProductNumT){
		cmProductNumTDao.save(cmProductNumT);
	}
	
	@Override
	public void update(CmProductNumTEntity cmProductNumT){
		cmProductNumTDao.update(cmProductNumT);
	}
	
	@Override
	public void delete(Long productNumId){
		cmProductNumTDao.delete(productNumId);
	}
	
	@Override
	public void deleteBatch(Long[] productNumIds){
		cmProductNumTDao.deleteBatch(productNumIds);
	}

	@Override
	public List<Map<String, String>> getqueryList(Map<String, Object> map) {
		return cmProductNumTDao.getqueryList(map);
	}
	
}
