package io.renren.modules.generator.entity;

import java.io.Serializable;
import java.util.Date;


/**
 * 机台号表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-10-22 16:45:59
 */
public class CmSelectListEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//机台号id
	private String value;
	//备注
	private String remark;
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

}
