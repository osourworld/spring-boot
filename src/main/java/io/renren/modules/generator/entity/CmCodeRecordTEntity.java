package io.renren.modules.generator.entity;

import java.io.Serializable;
import java.util.Date;


/**
 * 条码扫描记录表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-09-16 16:59:21
 */
public class CmCodeRecordTEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//条码记录id
	private Long codeRecordId;
	//条码
	private String code;
	//供应商编码
	private String supplierCode;
	//物料编码
	private String materialCode;
	//物料版本
	private String materialVersion;
	//创建人
	private Long createBy;
	//创建时间
	private Date createTime;
	
	private String cncProductTime;
	
	private String cncProductNum;
	
	private String outlookProductor;
	
	private String outlookDate;
	
	

	public String getCncProductTime() {
		return cncProductTime;
	}
	public void setCncProductTime(String cncProductTime) {
		this.cncProductTime = cncProductTime;
	}
	public String getCncProductNum() {
		return cncProductNum;
	}
	public void setCncProductNum(String cncProductNum) {
		this.cncProductNum = cncProductNum;
	}
	public String getOutlookProductor() {
		return outlookProductor;
	}
	public void setOutlookProductor(String outlookProductor) {
		this.outlookProductor = outlookProductor;
	}
	public String getOutlookDate() {
		return outlookDate;
	}
	public void setOutlookDate(String outlookDate) {
		this.outlookDate = outlookDate;
	}
	/**
	 * 设置：条码记录id
	 */
	public void setCodeRecordId(Long codeRecordId) {
		this.codeRecordId = codeRecordId;
	}
	/**
	 * 获取：条码记录id
	 */
	public Long getCodeRecordId() {
		return codeRecordId;
	}
	/**
	 * 设置：条码
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * 获取：条码
	 */
	public String getCode() {
		return code;
	}
	/**
	 * 设置：供应商编码
	 */
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	/**
	 * 获取：供应商编码
	 */
	public String getSupplierCode() {
		return supplierCode;
	}
	/**
	 * 设置：物料编码
	 */
	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}
	/**
	 * 获取：物料编码
	 */
	public String getMaterialCode() {
		return materialCode;
	}
	/**
	 * 设置：物料版本
	 */
	public void setMaterialVersion(String materialVersion) {
		this.materialVersion = materialVersion;
	}
	/**
	 * 获取：物料版本
	 */
	public String getMaterialVersion() {
		return materialVersion;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public Long getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
