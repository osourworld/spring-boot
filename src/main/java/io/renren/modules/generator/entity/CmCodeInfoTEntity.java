package io.renren.modules.generator.entity;

import java.io.Serializable;
import java.util.Date;


/**
 * 条码信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-09-16 16:59:21
 */
public class CmCodeInfoTEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//条码id
	private Long codeId;
	//条码
	private String code;
	//供应商编码
	private String supplierCode;
	//物料编码
	private String materialCode;
	//物料版本
	private String materialVersion;
	//采购单号
	private String purchaseNum;
	//进料单号
	private String incomeNum;
	//数量
	private Long orderNum;
	//生产日期
	private Date productTime;
	//生产日期
	private String productTimeStr;
	//检验员
	private String checkPerson;
	//打印个数
	private Long printNum;
	//备注
	private String remark;
	//有效标识（1：有效0：无效）
	private Integer validFlag;
	//创建人
	private Long createBy;
	//创建时间
	private Date createTime;
	
	//条码2
	private String code2;
	//关联条码
	private String relCode;
	//生产日期
	private String productDate;
	//出货日期
	private String orderDate;
	//生产流水号
	private Integer productBatchNum;
	//出货流水号
	private Integer orderBatchNum;
	
	

	public String getCode2() {
		return code2;
	}
	public void setCode2(String code2) {
		this.code2 = code2;
	}
	public String getRelCode() {
		return relCode;
	}
	public void setRelCode(String relCode) {
		this.relCode = relCode;
	}
	public String getProductDate() {
		return productDate;
	}
	public void setProductDate(String productDate) {
		this.productDate = productDate;
	}
	public String getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}
	public Integer getProductBatchNum() {
		return productBatchNum;
	}
	public void setProductBatchNum(Integer productBatchNum) {
		this.productBatchNum = productBatchNum;
	}
	public Integer getOrderBatchNum() {
		return orderBatchNum;
	}
	public void setOrderBatchNum(Integer orderBatchNum) {
		this.orderBatchNum = orderBatchNum;
	}
	public String getProductTimeStr() {
		return productTimeStr;
	}
	public void setProductTimeStr(String productTimeStr) {
		this.productTimeStr = productTimeStr;
	}
	/**
	 * 设置：条码id
	 */
	public void setCodeId(Long codeId) {
		this.codeId = codeId;
	}
	/**
	 * 获取：条码id
	 */
	public Long getCodeId() {
		return codeId;
	}
	/**
	 * 设置：条码
	 */
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * 获取：条码
	 */
	public String getCode() {
		return code;
	}
	/**
	 * 设置：供应商编码
	 */
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	/**
	 * 获取：供应商编码
	 */
	public String getSupplierCode() {
		return supplierCode;
	}
	/**
	 * 设置：物料编码
	 */
	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}
	/**
	 * 获取：物料编码
	 */
	public String getMaterialCode() {
		return materialCode;
	}
	/**
	 * 设置：物料版本
	 */
	public void setMaterialVersion(String materialVersion) {
		this.materialVersion = materialVersion;
	}
	/**
	 * 获取：物料版本
	 */
	public String getMaterialVersion() {
		return materialVersion;
	}
	/**
	 * 设置：采购单号
	 */
	public void setPurchaseNum(String purchaseNum) {
		this.purchaseNum = purchaseNum;
	}
	/**
	 * 获取：采购单号
	 */
	public String getPurchaseNum() {
		return purchaseNum;
	}
	/**
	 * 设置：进料单号
	 */
	public void setIncomeNum(String incomeNum) {
		this.incomeNum = incomeNum;
	}
	/**
	 * 获取：进料单号
	 */
	public String getIncomeNum() {
		return incomeNum;
	}
	/**
	 * 设置：数量
	 */
	public void setOrderNum(Long orderNum) {
		this.orderNum = orderNum;
	}
	/**
	 * 获取：数量
	 */
	public Long getOrderNum() {
		return orderNum;
	}
	/**
	 * 设置：生产日期
	 */
	public void setProductTime(Date productTime) {
		this.productTime = productTime;
	}
	/**
	 * 获取：生产日期
	 */
	public Date getProductTime() {
		return productTime;
	}
	/**
	 * 设置：检验员
	 */
	public void setCheckPerson(String checkPerson) {
		this.checkPerson = checkPerson;
	}
	/**
	 * 获取：检验员
	 */
	public String getCheckPerson() {
		return checkPerson;
	}
	/**
	 * 设置：打印个数
	 */
	public void setPrintNum(Long printNum) {
		this.printNum = printNum;
	}
	/**
	 * 获取：打印个数
	 */
	public Long getPrintNum() {
		return printNum;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：有效标识（1：有效0：无效）
	 */
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}
	/**
	 * 获取：有效标识（1：有效0：无效）
	 */
	public Integer getValidFlag() {
		return validFlag;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public Long getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
