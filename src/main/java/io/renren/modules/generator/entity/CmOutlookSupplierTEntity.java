package io.renren.modules.generator.entity;

import java.io.Serializable;
import java.util.Date;


/**
 * 表面处理厂家
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-10-22 16:45:59
 */
public class CmOutlookSupplierTEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//表面处理厂家id
	private Long outlookSupplierId;
	//表面处理厂家
	private String outlookSupplier;
	//备注
	private String remark;
	//创建人
	private Long createBy;
	//创建时间
	private Date createTime;

	/**
	 * 设置：表面处理厂家id
	 */
	public void setOutlookSupplierId(Long outlookSupplierId) {
		this.outlookSupplierId = outlookSupplierId;
	}
	/**
	 * 获取：表面处理厂家id
	 */
	public Long getOutlookSupplierId() {
		return outlookSupplierId;
	}
	/**
	 * 设置：表面处理厂家
	 */
	public void setOutlookSupplier(String outlookSupplier) {
		this.outlookSupplier = outlookSupplier;
	}
	/**
	 * 获取：表面处理厂家
	 */
	public String getOutlookSupplier() {
		return outlookSupplier;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public Long getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
}
