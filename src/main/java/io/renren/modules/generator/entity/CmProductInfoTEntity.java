package io.renren.modules.generator.entity;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;


/**
 * 产品信息表
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2017-09-16 16:59:21
 */
public class CmProductInfoTEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//产品id
	private Long productId;
	//供应商编码
	private String supplierCode;
	//物料编码
	private String materialCode;
	//物料名称
	private String materialName;
	//内部物料编码
	private String innerMaterialCode;
	//图号
	private String imgCode;
	//物料版本
	private String materialVersion;
	//板材型号
	private String model;
	//铜箔厚度
	private String copperThick;
	//介质厚度
	private String mediumThick;
	//产品类型（1：线路板2：非线路板）
	private Integer productType;
	//有效标识（1：有效0：无效）
	private Integer validFlag;
	//创建人
	private Long createBy;
	//创建时间
	private Date createTime;
	//备注
	private String remark;

	/**
	 * 设置：产品id
	 */
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	/**
	 * 获取：产品id
	 */
	public Long getProductId() {
		return productId;
	}
	/**
	 * 设置：供应商编码
	 */
	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}
	/**
	 * 获取：供应商编码
	 */
	public String getSupplierCode() {
		return supplierCode;
	}
	/**
	 * 设置：物料编码
	 */
	public void setMaterialCode(String materialCode) {
		this.materialCode = materialCode;
	}
	/**
	 * 获取：物料编码
	 */
	public String getMaterialCode() {
		return materialCode;
	}
	/**
	 * 设置：物料名称
	 */
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	/**
	 * 获取：物料名称
	 */
	public String getMaterialName() {
		return materialName;
	}
	/**
	 * 设置：内部物料编码
	 */
	public void setInnerMaterialCode(String innerMaterialCode) {
		this.innerMaterialCode = innerMaterialCode;
	}
	/**
	 * 获取：内部物料编码
	 */
	public String getInnerMaterialCode() {
		return innerMaterialCode;
	}
	/**
	 * 设置：图号
	 */
	public void setImgCode(String imgCode) {
		this.imgCode = imgCode;
	}
	/**
	 * 获取：图号
	 */
	public String getImgCode() {
		return imgCode;
	}
	/**
	 * 设置：物料版本
	 */
	public void setMaterialVersion(String materialVersion) {
		this.materialVersion = materialVersion;
	}
	/**
	 * 获取：物料版本
	 */
	public String getMaterialVersion() {
		return materialVersion;
	}
	/**
	 * 设置：板材型号
	 */
	public void setModel(String model) {
		this.model = model;
	}
	/**
	 * 获取：板材型号
	 */
	public String getModel() {
		return model;
	}
	/**
	 * 设置：铜箔厚度
	 */
	public void setCopperThick(String copperThick) {
		this.copperThick = copperThick;
	}
	/**
	 * 获取：铜箔厚度
	 */
	public String getCopperThick() {
		return copperThick;
	}
	/**
	 * 设置：介质厚度
	 */
	public void setMediumThick(String mediumThick) {
		this.mediumThick = mediumThick;
	}
	/**
	 * 获取：介质厚度
	 */
	public String getMediumThick() {
		return mediumThick;
	}
	/**
	 * 设置：产品类型（1：线路板2：非线路板）
	 */
	public void setProductType(Integer productType) {
		this.productType = productType;
	}
	/**
	 * 获取：产品类型（1：线路板2：非线路板）
	 */
	public Integer getProductType() {
		return productType;
	}
	/**
	 * 设置：有效标识（1：有效0：无效）
	 */
	public void setValidFlag(Integer validFlag) {
		this.validFlag = validFlag;
	}
	/**
	 * 获取：有效标识（1：有效0：无效）
	 */
	public Integer getValidFlag() {
		return validFlag;
	}
	/**
	 * 设置：创建人
	 */
	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}
	/**
	 * 获取：创建人
	 */
	public Long getCreateBy() {
		return createBy;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateTime() {
		return createTime;
	}
	/**
	 * 设置：备注
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}
	/**
	 * 获取：备注
	 */
	public String getRemark() {
		return remark;
	}
}
