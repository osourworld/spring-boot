$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'generator/cmcoderecordt/list',
        datatype: "json",
        colModel: [			
			{ label: 'codeRecordId', name: 'codeRecordId', hidden:true, index: 'code_record_id', width: 50, key: true },
			{ label: '条码', name: 'code', index: 'code_', width: 80 }, 
			{ label: '供方代码', name: 'supplierCode', index: 'supplier_code', width: 80 }, 			
			{ label: '物料编码', name: 'materialCode', index: 'material_code', width: 80 }, 			
			{ label: '物料版本', name: 'materialVersion', index: 'material_version', width: 80 },
			{ label: 'CNC加工日期', name: 'cncProductTime', width: 80 }, 
			{ label: 'CNC加工机台号', name: 'cncProductNum', width: 80 }, 
			{ label: '表面处理厂家', name: 'outlookProductor', width: 80 }, 
			{ label: '表面处理日期', name: 'outlookDate', width: 80 }, 
			{ label: '创建时间', name: 'createTime', index: 'create_time', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		cmCodeRecordT: {},
		supplierCode:"",
		materialCode:"",
		startTime:"",
		endTime:"",
		materialVersion:"",
		cncProductTime:"",
		productTime:"",
		cncProductNum:"",
		outlookProductor:"",
		outlookDate:"",
		outlookHandleTime:"",
		code:""
			
	},
	methods: {
		searchInfo:function(){
			vm.reload();
		},
		resetInfo:function(){
			this.supplierCode="";
			this.materialCode="";
			this.startTime="";
			this.endTime="";
			this.materialVersion="";
			
			this.cncProductTime="";
			this.productTime="";
			this.cncProductNum="";
			this.outlookProductor="";
			this.outlookDate="";
			this.outlookHandleTime="";
			this.code="";
			vm.reload();
		},
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.cmCodeRecordT = {};
		},
		update: function (event) {
			var codeRecordId = getSelectedRow();
			if(codeRecordId == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(codeRecordId)
		},
		saveOrUpdate: function (event) {
			var url = vm.cmCodeRecordT.codeRecordId == null ? "generator/cmcoderecordt/save" : "generator/cmcoderecordt/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.cmCodeRecordT),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var codeRecordIds = getSelectedRows();
			if(codeRecordIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "generator/cmcoderecordt/delete",
                    contentType: "application/json",
				    data: JSON.stringify(codeRecordIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(codeRecordId){
			$.get(baseURL + "generator/cmcoderecordt/info/"+codeRecordId, function(r){
                vm.cmCodeRecordT = r.cmCodeRecordT;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var param={};
			param.supplierCode=vm.supplierCode;
			param.materialCode=vm.materialCode;
			param.startTime=getFormatDate(vm.startTime);
			param.endTime=getFormatDate(vm.endTime);
			param.materialVersion=vm.materialVersion;
			
			param.cncProductTime=getFormatDate(vm.productTime,"/");
			param.cncProductNum=vm.cncProductNum;
			param.outlookProductor=vm.outlookProductor;
			param.outlookDate=getFormatDate(vm.outlookHandleTime,"/");
			param.code=vm.code;
			
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page,
                postData:param
            }).trigger("reloadGrid");
		}
	}
});

//日期格式化
function getFormatDate(date,seperator) {
    //var date = new Date();
	if(date==undefined || date==null || date==""){
		return "";
	}
    var seperator1 = (seperator!=undefined && seperator!=null)?seperator:"-";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = year + seperator1 + month + seperator1 + strDate;
    return currentdate;
}

