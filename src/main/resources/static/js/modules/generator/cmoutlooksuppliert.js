$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'generator/cmoutlooksuppliert/list',
        datatype: "json",
        colModel: [			
			{ label: 'outlookSupplierId',hidden:true, name: 'outlookSupplierId', index: 'outlook_supplier_id', width: 50, key: true },
			{ label: '表面处理厂家', name: 'outlookSupplier', index: 'outlook_supplier', width: 80 }, 			
			{ label: '备注', name: 'remark', index: 'remark', width: 80 }, 			
			{ label: '创建人', name: 'createBy',hidden:true, index: 'create_by', width: 80 }, 			
			{ label: '创建时间', name: 'createTime', index: 'create_time', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		cmOutlookSupplierT: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.cmOutlookSupplierT = {};
		},
		update: function (event) {
			var outlookSupplierId = getSelectedRow();
			if(outlookSupplierId == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(outlookSupplierId)
		},
		saveOrUpdate: function (event) {
			var url = vm.cmOutlookSupplierT.outlookSupplierId == null ? "generator/cmoutlooksuppliert/save" : "generator/cmoutlooksuppliert/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.cmOutlookSupplierT),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var outlookSupplierIds = getSelectedRows();
			if(outlookSupplierIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "generator/cmoutlooksuppliert/delete",
                    contentType: "application/json",
				    data: JSON.stringify(outlookSupplierIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(outlookSupplierId){
			$.get(baseURL + "generator/cmoutlooksuppliert/info/"+outlookSupplierId, function(r){
                vm.cmOutlookSupplierT = r.cmOutlookSupplierT;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});