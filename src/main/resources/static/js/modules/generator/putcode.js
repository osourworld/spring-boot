
var vm = new Vue({
	el:'#rrapp',
	data:{
		title: "条码防重号检验",
		code: "",
		supplierCode:"",
		materialCode:"",
		materialVersion:"",
		incomeBatchNo:"",
		cncProductTime:"",
		productTime:"",
		outlookHandleTime:"",
		cncProductNum:"",
		outlookProductor:"",
		outlookTime:"",
		putFlag:true,
		restaurants: [],
		restaurants2: [],
		outlookSupplier: '',
		user:{}
	},
	mounted: function() {
	      this.restaurants = this.loadAll();
	      this.restaurants2 = this.loadAll2();
	      this.getUser();
	},
	methods: {
		getUser: function(){
			var this_=this;
			$.getJSON(baseURL + "sys/user/getinfo", function(r){
				this_.user = r.user;
			});
		},
		standardBarCode:function(param){
			//param="A011 3PB0000013 H2200001";
			//param="A094 3555100914 B H3500981";
			var barCode=param.toUpperCase();
			var nospace = barCode.replace(/\s/g, '');
			var len=nospace.length;
			if(len>18){
				var nospace = barCode.replace(/\s/g, '');
				var index10=nospace.substring(len-10,len-9);
				var index11=nospace.substring(len-11,len-10);
				var incomeBatchNo="";
				var materialVersion="";
				var materialCode="";
				var supplierCode="";
				if(index10=="A" || index10=="V"){
					incomeBatchNo=nospace.substring(len-7,len);
					materialVersion=nospace.substring(len-10,len-7);
					materialCode=nospace.substring(len-20,len-10);
					supplierCode=nospace.substring(0,len-20);
				}else if(index11=="A" || index11=="V"){
					incomeBatchNo=nospace.substring(len-8,len);
					materialVersion=nospace.substring(len-11,len-8);
					materialCode=nospace.substring(len-21,len-11);
					supplierCode=nospace.substring(0,len-21);
				}else{
					incomeBatchNo=nospace.substring(len-8,len);
					var index8 = nospace.substring(len-9,len-8);
					materialVersion=index8;
					materialCode=nospace.substring(len-19,len-9);
					supplierCode=nospace.substring(0,len-19);
//					incomeBatchNo=nospace.substring(len-8,len);
//					materialCode=nospace.substring(len-18,len-8);
//					supplierCode=nospace.substring(0,len-18);
//					materialVersion='A00';
				}
				if(materialVersion=='A00'){
					var resultCode=supplierCode+" "+materialCode+" "+incomeBatchNo;
				}else{
					var resultCode=supplierCode+" "+materialCode+" "+materialVersion+" "+incomeBatchNo;
				}
				
				//$("#barcodeInfo").val(resultCode.trim());
				this.materialCode=materialCode;
				this.materialVersion=materialVersion;
				this.supplierCode=supplierCode;
				this.incomeBatchNo=incomeBatchNo;
			}
		},
		querySearch: function(queryString, cb) {
	        var restaurants = this.restaurants;
	        var results = queryString ? restaurants.filter(this.createFilter(queryString)) : restaurants;
	        // 调用 callback 返回建议列表的数据
	        cb(results);
	      },
	      createFilter: function(queryString) {
	        return function a(restaurant){
	          return (restaurant.value.indexOf(queryString) === 0);
	        };
	      },
	      loadAll: function() {
	    	  var outlooksupplierList=[];
	    	  var pageParam={};
	    	  pageParam.page=1;
	    	  pageParam.limit=999999;
	    	  var url = 'generator/cmproductnumt/getlist';
	    	  $.ajaxSetup({
	    		  async: false
	    		  });
				$.ajax({
					type: "GET",
				    url: baseURL + url,
	                contentType: "application/json",
				    data: pageParam,
				    success: function(r){
				    	if(r.code === 0){
				    		outlooksupplierList=r.list;
						}else{
							alert(r.msg);
						}
					}
				});
				console.log(outlooksupplierList);
	        return outlooksupplierList;
	      },
	      handleSelect: function(item) {
	        console.log(item);
	      },
	      querySearch2: function(queryString, cb) {
		        var restaurants = this.restaurants2;
		        var results = queryString ? restaurants.filter(this.createFilter2(queryString)) : restaurants;
		        // 调用 callback 返回建议列表的数据
		        cb(results);
		      },
		      createFilter2: function(queryString) {
		        return function a(restaurant){
		          return (restaurant.value.indexOf(queryString) === 0);
		        };
		      },
		      loadAll2: function() {
		    	  var productNumList=[];
		    	  var pageParam={};
		    	  pageParam.page=1;
		    	  pageParam.limit=999999;
		    	  var url = 'generator/cmoutlooksuppliert/getlist';
		    	  $.ajaxSetup({
		    		  async: false
		    		  });
					$.ajax({
						type: "GET",
					    url: baseURL + url,
		                contentType: "application/json",
					    data: pageParam,
					    success: function(r){
					    	if(r.code === 0){
					    		productNumList=r.list;
							}else{
								alert(r.msg);
							}
						}
					});
					console.log(productNumList);
		        return productNumList;
		      },
		checkCode: function(){
			if(this.code==null || this.code=="" || this.code==undefined){
				this.$message({
			          message: '条码不能为空',
			          type: 'warning'
			        });
				return;
			}
			var codetrim=(this.code).trim();
			if(codetrim==""){
				this.$message({
			          message: '条码不能为空',
			          type: 'warning'
			        });
				return;
			}
			this.standardBarCode(codetrim);
			var param={};
			param.code=codetrim;
			param.materialCode=this.materialCode;
			param.materialVersion=this.materialVersion;
			param.supplierCode=this.supplierCode;
			param.incomeBatchNo=this.incomeBatchNo;
			
			param.cncProductTime=getFormatDate(this.productTime);
			param.cncProductNum=this.cncProductNum;
			param.outlookProductor=this.outlookProductor;
			if(this.outlookHandleTime != undefined && this.outlookHandleTime!=""){
				if(getDateDiff(this.outlookHandleTime)>30*6){
					alert("表面处理已超期！");
					return;
				}
				param.outlookDate=getFormatDate(this.outlookHandleTime);
			}
			
			if(param.cncProductTime==null || param.cncProductTime=="" || param.cncProductTime==undefined){
				this.$message({
			          message: '加工日期不能为空',
			          type: 'warning'
			        });
				return;
			}
			if(param.cncProductNum==null || param.cncProductNum=="" || param.cncProductNum==undefined){
				this.$message({
			          message: '加工机台号不能为空',
			          type: 'warning'
			        });
				return;
			}
			if(param.outlookProductor==null || param.outlookProductor=="" || param.outlookProductor==undefined){
				this.$message({
			          message: '表面处理厂家不能为空',
			          type: 'warning'
			        });
				return;
			}
			if(param.outlookDate==null || param.outlookDate=="" || param.outlookDate==undefined){
				this.$message({
			          message: '表面处理日期不能为空',
			          type: 'warning'
			        });
				return;
			}
			//防止供方代码错误
			if(param.supplierCode!=this.user.name){
				this.$message.error('错了哦，登录用户的供应商编码不一致');
				return;
			}
			//防止重复提交
			if(!this.putFlag){
				return;
			}
		   var this_=this;
      	   this.putFlag=false;
				$.ajax({
					type: "POST",
				    url: baseURL + "generator/cmcoderecordt/checkinfo",
				    data: JSON.stringify(param),
				    contentType: "application/json",
				    success: function(r){
				    	vm.putFlag=true;
				    	if(r.code === 0){
				    		if(r.codeCount===0 && r.saveStatus===1){
				    			//alert("记录成功");
				    			this_.$message({
				    		          message: '恭喜你，记录成功',
				    		          type: 'success'
				    		        });
				    		}else if(r.codeCount>0){
				    			//alert("条码重复,上次扫描时间："+r.time);
				    			this_.$message({
				    		          message: "条码重复,上次扫描时间："+r.time,
				    		          type: 'warning'
				    		        });
				    		}
						}else{
							//alert(r.msg);
							this_.$message.error(r.msg);
						}
				    	//清空输入
				    	this_.code="";
				    	this_.supplierCode="";
				    	this_.materialCode="";
				    	this_.materialVersion="";
				    	this_.incomeBatchNo="";
				    	this_.cncProductTime="";
				    	this_.productTime="";
				    	this_.outlookHandleTime="";
				    	this_.cncProductNum="";
				    	this_.outlookProductor="";
				    	this_.outlookTime="";
				    	this_.outlookSupplier="";
					}
				});
        
			 
			
		}
	}
});

$("#codeInput").focus();

//日期格式化
function getFormatDate(date) {
    //var date = new Date();
	if(date==undefined || date==null || date==""){
		return "";
	}
    var seperator1 = "/";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = year + seperator1 + month + seperator1 + strDate;
    return currentdate;
}

//计算两个日期的间隔天数
function getDateDiff(startDate)  
{  
    var startTime = startDate.getTime();     
    var endTime = new Date().getTime(); 
    if((endTime - startTime)<0){
    	return 0;
    }
    var dates = Math.abs((endTime - startTime))/(1000*60*60*24);     
    return  dates;    
}