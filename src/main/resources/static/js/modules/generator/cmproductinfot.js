var productType = T.p("productType");
console.log("productType>>>>"+productType);

$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'generator/cmproductinfot/list',
        datatype: "json",
        postData:{
            "productType":productType
        },
        colModel: [			
			{ label: 'productId',hidden:true, name: 'productId', index: 'product_id', width: 50, key: true },
			{ label: '供应商编码', name: 'supplierCode', index: 'supplier_code', width: 80 }, 			
			{ label: '物料编码', name: 'materialCode', index: 'material_code', width: 80 }, 			
			{ label: '物料名称', name: 'materialName', index: 'material_name', width: 80 }, 			
			{ label: '内部物料编码', name: 'innerMaterialCode', index: 'inner_material_code', width: 80 }, 			
			{ label: '图号', name: 'imgCode', index: 'img_code', width: 80 }, 			
			{ label: '物料版本', name: 'materialVersion', index: 'material_version', width: 80 }, 			
			{ label: '板材型号',hidden:productType==="2", name: 'model', index: 'model_', width: 80 }, 			
			{ label: '铜箔厚度',hidden:productType==="2", name: 'copperThick', index: 'copper_thick', width: 80 }, 			
			{ label: '介质厚度',hidden:productType==="2", name: 'mediumThick', index: 'medium_thick', width: 80 }, 			
			{ label: '创建时间', name: 'createTime', index: 'create_time', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		headTitle:null,
		cmProductInfoT: {},
		innerMaterialCode:"",
		materialCode:"",
		materialName:"",
		materialVersion:""
	},
	methods: {
		searchInfo:function(){
			vm.reload();
		},
		resetInfo:function(){
			this.innerMaterialCode="";
			this.materialCode="";
			this.materialName="";
			this.materialVersion="";
			vm.reload();
		},
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			if(productType==="1"){
				vm.headTitle="新建产品信息（线路板类）";
			}else if(productType==="2"){
				vm.headTitle="新建产品信息（非线路板类）";
			}
			
			vm.cmProductInfoT = {};
		},
		update: function (event) {
			var productId = getSelectedRow();
			if(productId == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(productId)
		},
		saveOrUpdate: function (event) {
			var passFlag=$("#jsForm").valid();
			if(!passFlag){
				return;
			}
			
			
			vm.cmProductInfoT.productType=productType;
			var url = vm.cmProductInfoT.productId == null ? "generator/cmproductinfot/save" : "generator/cmproductinfot/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.cmProductInfoT),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var productIds = getSelectedRows();
			if(productIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "generator/cmproductinfot/delete",
                    contentType: "application/json",
				    data: JSON.stringify(productIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(productId){
			$.get(baseURL + "generator/cmproductinfot/info/"+productId, function(r){
                vm.cmProductInfoT = r.cmProductInfoT;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                "page":page,
                postData:{
                "productType":productType,
                "innerMaterialCode":vm.innerMaterialCode==undefined?"":(vm.innerMaterialCode).trim(),
        		"materialCode":vm.materialCode==undefined?"":(vm.materialCode).trim(),
        		"materialName":vm.materialName==undefined?"":(vm.materialName).trim(),
        		"materialVersion":vm.materialVersion==undefined?"":(vm.materialVersion).trim()
                }
            }).trigger("reloadGrid");
		}
	}
});