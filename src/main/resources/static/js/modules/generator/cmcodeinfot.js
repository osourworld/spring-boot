$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'generator/cmcodeinfot/list',
        datatype: "json",
        colModel: [			
			{ label: 'codeId', name: 'codeId', hidden:true,index: 'code_id', width: 50, key: true },
			{ label: '条码', name: 'code', index: 'code_', width: 80 }, 			
			{ label: '供应商编码', name: 'supplierCode', index: 'supplier_code', width: 80 }, 			
			{ label: '物料编码', name: 'materialCode', index: 'material_code', width: 80 }, 			
			{ label: '物料版本', name: 'materialVersion', index: 'material_version', width: 80 }, 			
			{ label: '采购单号', name: 'purchaseNum', index: 'purchase_num', width: 80 }, 			
			{ label: '进料单号', name: 'incomeNum', index: 'income_num', width: 80 }, 			
			{ label: '数量', name: 'orderNum', index: 'order_num', width: 80 }, 			
			{ label: '生产日期', name: 'productTime', index: 'product_time', width: 80 }, 			
			{ label: '检验员', name: 'checkPerson', index: 'check_person', width: 80 }, 			
			{ label: '打印个数', name: 'printNum', index: 'print_num', width: 80 }, 			
			{ label: '创建人', name: 'createBy', hidden:true, index: 'create_by', width: 80 }, 			
			{ label: '创建时间', name: 'createTime', index: 'create_time', width: 80 }			
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$("#jqGrid").closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		cmCodeInfoT: {}
	},
	methods: {
		query: function () {
			vm.reload();
		},
		add: function(){
			vm.showList = false;
			vm.title = "新增";
			vm.cmCodeInfoT = {};
		},
		update: function (event) {
			var codeId = getSelectedRow();
			if(codeId == null){
				return ;
			}
			vm.showList = false;
            vm.title = "修改";
            
            vm.getInfo(codeId)
		},
		saveOrUpdate: function (event) {
			var url = vm.cmCodeInfoT.codeId == null ? "generator/cmcodeinfot/save" : "generator/cmcodeinfot/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.cmCodeInfoT),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var codeIds = getSelectedRows();
			if(codeIds == null){
				return ;
			}
			
			confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: baseURL + "generator/cmcodeinfot/delete",
                    contentType: "application/json",
				    data: JSON.stringify(codeIds),
				    success: function(r){
						if(r.code == 0){
							alert('操作成功', function(index){
								$("#jqGrid").trigger("reloadGrid");
							});
						}else{
							alert(r.msg);
						}
					}
				});
			});
		},
		getInfo: function(codeId){
			$.get(baseURL + "generator/cmcodeinfot/info/"+codeId, function(r){
                vm.cmCodeInfoT = r.cmCodeInfoT;
            });
		},
		reload: function (event) {
			vm.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		}
	}
});