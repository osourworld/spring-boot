var productType = T.p("productType");
console.log("productType>>>>"+productType);

var vm = new Vue({
	el:'#rrapp',
	data:{
		cmCodeInfoT: {},
		materialVersion:"",
		versionList:[],
		showError:false,
		productTime:"",
		productTimeIsNull:false,
		material:"",
		batchNum:"",
		barcodeInfo1:"",
		barcodeInfo2:"",
		pickerOptions: {
	          disabledDate(time) {
	            return time.getTime() > Date.now();
	          },
	          shortcuts: [{
	            text: '今天',
	            onClick(picker) {
	              picker.$emit('pick', new Date());
	            }
	          }, {
	            text: '昨天',
	            onClick(picker) {
	              const date = new Date();
	              date.setTime(date.getTime() - 3600 * 1000 * 24);
	              picker.$emit('pick', date);
	            }
	          }, {
	            text: '一周前',
	            onClick(picker) {
	              const date = new Date();
	              date.setTime(date.getTime() - 3600 * 1000 * 24 * 7);
	              picker.$emit('pick', date);
	            }
	          }]
	        }
	},
	created: function() {
	    this.findCodeVersion();
    },
	methods: {
		printCode: function (event) {
			var passFlag=$("#jsForm").valid();
			if(!passFlag){
				return;
			}
			
			var codeInfo={};
			codeInfo.code=this.cmCodeInfoT.code;
			codeInfo.supplierCode=this.cmCodeInfoT.supplierCode;
			codeInfo.materialVersion=this.materialVersion;
			codeInfo.materialCode=this.cmCodeInfoT.materialCode;
			codeInfo.purchaseNum=this.cmCodeInfoT.purchaseNum;
			codeInfo.incomeNum=this.cmCodeInfoT.incomeNum;
			codeInfo.orderNum=this.cmCodeInfoT.orderNum;
			codeInfo.productTimeStr=getNowFormatDate(this.productTime);
			codeInfo.checkPerson=this.cmCodeInfoT.checkPerson;
			codeInfo.printNum=this.cmCodeInfoT.printNum;
			codeInfo.productType=this.cmCodeInfoT.productType;
			
			codeInfo.materialName=this.cmCodeInfoT.materialName;
			codeInfo.imgCode=this.cmCodeInfoT.imgCode;
			codeInfo.model=this.cmCodeInfoT.model;
			codeInfo.copperThick=this.cmCodeInfoT.copperThick;
			codeInfo.mediumThick=this.cmCodeInfoT.mediumThick;
			
			codeInfo.relCode=this.cmCodeInfoT.relCode;
			codeInfo.productDate=this.createDateBatchNum(1);
			codeInfo.orderDate=this.createDateBatchNum(2);
			
			if(codeInfo.productTimeStr==null){
				this.productTimeIsNull=true;
				return;
			}else{
				this.productTimeIsNull=false;
			}
			//生产条码
			this.standardBarCode(1);
			this.standardBarCode(2);
			console.log(this.barcodeInfo1+">>"+this.barcodeInfo2);
			codeInfo.barcodeInfo1=this.barcodeInfo1;
			codeInfo.barcodeInfo2=this.barcodeInfo2;
			codeInfo.code=this.barcodeInfo1+";"+this.barcodeInfo2;
			
			var url = "generator/cmcodeinfot/save";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(codeInfo),
			    success: function(r){
			    	if(r.code === 0){
			    		var code1=r.code1;
			    		var code2=r.code2;
			    		codeInfo.barcodeInfo1=code1;
						codeInfo.barcodeInfo2=code2;
						codeInfo.code=code1+";"+code2;
			    		window.open("./printview.html?codeInfo="+escape(JSON.stringify(codeInfo)));
			    		//vm.clickOpenWin("./printview.html?codeInfo="+escape(JSON.stringify(codeInfo)));
					}else{
						alert(r.msg);
					}
				}
			});
		},
		clickOpenWin: function(f){
		    var dataKey = "clickOpenWin.dataKey"
		    var me = $(this);
		    var A = me.data(dataKey);
		    var returnData = null;
		    if(!A){
		        A = $("");
		        me.data(dataKey, A);
		        A.click(function(e){
		            if(returnData){
		                A.attr("href", returnData);
		            }else {
		                A.before(me);
		                e.stop();
		            }
		        });
		    }
		    me.mouseover(function(){$(this).before(A).appendTo(A);});
		    me.mouseout(function(){A.before($(this));});
		    me.click(function(){
		        A.attr("href", "#|");
		        returnData = f.apply(this, arguments);
		    });
		},
		payWX: function () {
			alert("success");
			return;
			var url = "generator/cmproductinfot/update";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(vm.cmProductInfoT),
			    success: function(r){
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				}
			});
		},
		findCodeVersion:function(){
			var url = "generator/cmcodeinfot/codeversionlist";
			$.ajax({
				type: "GET",
			    url: baseURL + url,
                contentType: "application/json",
			    data: "",
			    success: function(r){
			    	if(r.code === 0){
						vm.versionList=r.result;
						vm.materialVersion=vm.versionList[0];
					}else{
						alert(r.msg);
					}
				}
			});
		},
		equalCodeInfo: function () {
			var innerCode=this.cmCodeInfoT.innerMaterialCode;
			var codeVersion=this.materialVersion;
			if(innerCode=="" || innerCode==undefined || codeVersion=="" || codeVersion==undefined){
				return;
			}
			var paramData={};
			paramData.innerMaterialCode=innerCode;
			paramData.materialVersion=codeVersion;
			var url = "generator/cmproductinfot/equalCodeInfo";
			$.ajax({
				type: "POST",
			    url: baseURL + url,
                contentType: "application/json",
			    data: JSON.stringify(paramData),
			    success: function(r){
			    	if(r.code === 0){
						console.log(r);
						if(r.cmProductInfoT!=null){
							vm.cmCodeInfoT=r.cmProductInfoT;
							vm.showError=false;
						}else{
							vm.showError=true;
							vm.cmCodeInfoT.materialCode="";
						}
					}else{
						alert(r.msg);
					}
				}
			});
			
		},
		//生成条码
		createDateBatchNum:function(type){
			var dateBatchNum="";
			var nowDate=new Date();
			var year=getYearParam(nowDate);
			var month = nowDate.getMonth() + 1;
		    var strDate = nowDate.getDate();
		    if (month >= 1 && month <= 9) {
		        month = "0" + month;
		    }
		    if (strDate >= 0 && strDate <= 9) {
		        strDate = "0" + strDate;
		    }
			var weeks=getYearWeek(nowDate);
			if(type===1){
				dateBatchNum=year+weeks;
			}else{
				dateBatchNum=year+month+strDate;
			}
			return dateBatchNum;
		},
		//生成条码
		createBatchNum:function(type,num){
			var nowDate=new Date();
			var year=getYearParam(nowDate);
			var month = nowDate.getMonth() + 1;
		    var strDate = nowDate.getDate();
		    if (month >= 1 && month <= 9) {
		        month = "0" + month;
		    }
		    if (strDate >= 0 && strDate <= 9) {
		        strDate = "0" + strDate;
		    }
			var weeks=getYearWeek(nowDate);
			if(type===1){
				this.batchNum=year+weeks+num;
			}else{
				this.batchNum=year+month+strDate+num;
			}
			return this.batchNum;
		},
		standardBarCode:function(type){
			if(type===1){
				var batchNum=this.createBatchNum(type,this.cmCodeInfoT.incomeNum);
				this.barcodeInfo1=this.cmCodeInfoT.supplierCode+this.cmCodeInfoT.materialCode+this.cmCodeInfoT.materialVersion+batchNum;
			}else{
				var batchNum=this.createBatchNum(type,this.cmCodeInfoT.incomeNum);
				this.barcodeInfo2=this.cmCodeInfoT.supplierCode+this.cmCodeInfoT.materialCode+this.cmCodeInfoT.materialVersion+batchNum;
			}
		}
	}
});

function getNowFormatDate(value) {
	if(value==null || value==undefined || value==""){
		return null;
	}
    var date = value;
    var seperator1 = "/";
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = year + seperator1 + month + seperator1 + strDate;
    return currentdate;
}

function getYearWeek(date){  
    var date2=new Date(date.getFullYear(), 0, 1);  
    var day1=date.getDay();  
    if(day1==0) day1=7;  
    var day2=date2.getDay();  
    if(day2==0) day2=7;  
    d = Math.round((date.getTime() - date2.getTime()+(day2-day1)*(24*60*60*1000)) / 86400000);    
    var weeks=Math.ceil(d /7)+1;
    if(weeks>=0 && weeks<=9){
    	weeks="0"+weeks;
    }
    return weeks;   
} 
function getYearParam(date){
	var yearMap=new Map();
	yearMap={"2009":"9","2010":"A","2011":"B","2012":"C","2013":"D","2014":"E","2015":"F","2016":"G","2017":"H","2018":"I","2019":"J","2020":"K","2021":"L","2022":"M","2023":"N","2024":"O","2025":"P","2026":"Q","2027":"R","2028":"S","2029":"T","2030":"U","2031":"V","2032":"W","2033":"X","2034":"Y","2035":"Z"};
	var year=date.getFullYear();
	return yearMap[year];
}

/* 质朴长存法 */
function pad(num, n) {
	var len = num.toString().length;
	while(len < n) {
		num = "0" + num;
		len++;
	}
	return num;
3}
